#include <stdio.h>

void greeting(const char* s)
{
  static int a = 0;
  a++;

  printf("%d'th occurence of greeting\n", a);
  printf("hello %s\n\n", s);
}
